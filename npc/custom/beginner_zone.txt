//Entering Beginner Zone

amatsu,225,254,4	script	Beginner Zone	759,{

	mes "Would you like to go to the Beginner Zone?";
	if(select("Yes", "No") == 2) close;
	if(class == 0 || class == 4001) {
	warp "force_1-1" ,25,24;
	} 
	else {
	mes "Only Novice and High Novice Can Enter This Room";
	close;
}
}
//The Exit to Amatsu

force_1-1.gat,25,26,3	script	Exit	759,{
	mes "Do You want to leave?";
	if(select("Yes", "No") == 2){
	next;
	mes "Then you can still leveling in this room";
	close;
	}else{
	warp "amatsu" ,224,252;
	}
	}
	
force_1-1,9,9,42,42	monster	Poring	1002,50,0,0,0
force_1-1,9,9,42,42	monster	Drops	1113,50,0,0,0
force_1-1,9,9,42,42	monster	Poporing	1031,50,0,0,0

//mapflag
force_1-1	mapflag	nowarp
force_1-1	mapflag	nowarpto
force_1-1	mapflag	noteleport
force_1-1	mapflag	nomobloot