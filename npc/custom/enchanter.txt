amatsu,224,234,5	script	Enchanter	825,{
	mes "[^0000FFItem Enchanter^000000]";
	mes "I can enchant your equipment.";
	next;
T_yes:
	mes "[^0000FFItem Enchanter^000000]";
	mes "What would you like to Enchant?";
	menu "Armor",T_armor;
	close;
T_no:
	mes "[^0000FFItem Enchanter^000000]";
	mes "Okay, Come back if you have it already.";
	close;
	
T_armor:
	if ( select ( "Yes", "No" ) == 2 ) close;
	next;
	mes "[^0000FFItem Enchanter^000000]";
	if ( !getequipisequiped( EQI_ARMOR ) ) {
		mes "You dont have any ^0000FFArmor^000000 that is being equipped.";
		close;
	}
	.@id = getequipid( EQI_ARMOR );
	.@ref = getequiprefinerycnt( EQI_ARMOR );
	.@card1 = getequipcardid( EQI_ARMOR, 0 );
	.@card2 = getequipcardid( EQI_ARMOR, 1 );
	.@card3 = getequipcardid( EQI_ARMOR, 2 );
	.@card4 = getequipcardid( EQI_ARMOR, 3 );
	if ( .@card1 == 255 || .@card1 == 254 ) {
		mes "I can't enchant a signed equipment.";
		close;
	}
	if ( !.@card4 )
		.@enchant = 0;
	else if ( !.@card3 )
		.@enchant = 1;
	else if ( !.@card2 )
		.@enchant = 2;
	else {
		mes "Sorry, this ^0000FFArmor^000000 has already been enchanted 3 times.";
		close;
	}
	mes "Please choose what you want to enchant on your item?";
	.@o = select( implode( .effect$, ":" ) ) -1;
	for( .@i = 1; .@i <= 3; .@i++ )
		.@menu$ = .@menu$ + .effect$[ .@o ] +" +"+ .@i +":";
	.@r = select( .@menu$ ) -1;
	next;
	mes "[^0000FFItem Enchanter^000000]";
	mes "Enchant your armor with "+ .effect$[ .@o ] +" +"+ ( .@r +1 ) +" will cost "+ .item_req0[ .@r ] +" "+ getitemname( .item_id[0] ) +" and "+ .item_req1[ .@r ] +" "+ getitemname( .item_id[1] ) +", shall we continue ?";
	if ( select ( "Yes", "No" ) == 2 ) close;
	next;
	mes "[^0000FFItem Enchanter^000000]";
	if ( countitem( .item_id[0] ) < .item_req0[ .@r ] || countitem( .item_id[1] ) < .item_req1[ .@r ] ) {
		mes "Sorry, you need "+ 
			( countitem( .item_id[0] ) < .item_req0[ .@r ] ? .item_req0[ .@r ] +" "+ getitemname( .item_id[0] ) : "" ) + 
			( countitem( .item_id[1] ) < .item_req1[ .@r ] ? .item_req1[ .@r ] +" "+ getitemname( .item_id[1] ) : "" ) + 
			" to enchant this armor.";
		close;
	}
	// .@rand = rand(.totalchance);
	// while ( ( .@rand = .@rand - .rate[.@r] ) >= 0 ) .@r++;
	// .@o = rand(0,5); // orb of str/int/dex ...
	delitem .item_id[0], .item_req0[ .@r ];
	delitem .item_id[1], .item_req1[ .@r ];
	if( .rate[ .@r ] < rand( 1,100 ) ) {
		mes "Sorry enchancement failed.";
		close;
	}
	delitem2 .@id, 1,1, .@ref, 0, .@card1, .@card2, .@card3, .@card4;
	if ( !.@card4 )
		getitem2 .@id, 1,1, .@ref, 0, .@card1, .@card2, .@card3, 4700 + .@o * 10 + .@r;
	else if ( !.@card3 )
		getitem2 .@id, 1,1, .@ref, 0, .@card1, .@card2, 4700 + .@o * 10 + .@r, .@card4;
	else
		getitem2 .@id, 1,1, .@ref, 0, .@card1, 4700 + .@o * 10 + .@r, .@card3, .@card4;
	equip .@id;
	mes "Armor Enchancement successful !";
	announce ""+strcharinfo(0)+" enchanted his ARMOR!",0;
	close;
OnInit:
//    waitingroom "Item Enchanter",0;
	setarray .rate, 50,30,20; // rate of enchant
	setarray .item_id, 7227, 6024; // items ID requirement use to refine
	setarray .item_req0, 3,5,7; // how much item_id[0] pc need
	setarray .item_req1, 1,3,5; // how much item_id[1] pc need
	setarray .effect$, "Str", "Agi", "Vit", "Int", "Dex", "Luk";
	// while ( .@i < 3 ) {
		// .totalchance = .totalchance + .rate[.@i];
		// .@i++;
	// }
	end;

}